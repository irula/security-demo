package com.zhm.springboot.domain;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by haiming.zhuang on 2016/7/12.
 */
public class UserUrls  implements Serializable {
    private static final long serialVersionUID = 8879714410481818086L;
    private Integer id;
    private Integer userid;
    private Integer urlsid;
    private Timestamp entry_date;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getUrlsid() {
        return urlsid;
    }

    public void setUrlsid(Integer urlsid) {
        this.urlsid = urlsid;
    }

    public Timestamp getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(Timestamp entry_date) {
        this.entry_date = entry_date;
    }
}
