package com.zhm;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by zhm on 16-7-13.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

    public SecurityInitializer() {
        super(SecurityConfig.class, RedisConfig.class);
    }
}